const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const devmode = process.env.NODE_ENV;

module.exports = {
  mode: devmode,
  performance: {
    maxEntrypointSize: 400000,
    maxAssetSize: 400000
  },
  optimization: {
    splitChunks: {
      minChunks: 1,
      name: true,
      cacheGroups: {
        //SCSS files have been replaced by Styled components
        // styles: {
        //     name: 'styles',
        //     test: /\.css$/,
        //     chunks: 'all',
        //     enforce: true
        // },
        vendor: {
          name: "vendor",
          test: /[\\/]node_modules[\\/]/,
          chunks: "all",
          enforce: true
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        query: {
          presets: ["es2015", "stage-0", "react"]
        }
      }
      //SCSS files have been replaced by Styled components
      //   ,
      //   {
      //     test: /\.scss$/,
      //     use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
      //   }
    ]
  },
  plugins: [
    //SCSS files have been replaced by Styled components
    // new MiniCssExtractPlugin({
    //   filename: "[name].css"
    // }),
    new HtmlWebpackPlugin({
      template: "./index.html"
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    contentBase:
      devmode == "production"
        ? path.resolve(__dirname, "dist")
        : path.resolve(__dirname, "/"),
    hot: true
  }
};

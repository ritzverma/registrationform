import React from "react";
import Welcome from "./components/welcome/welcome-component";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { StyledLink } from "./components/custom-components/custom-styled-component";

function asyncComponent(getComponent) {
  return class AsyncComponent extends React.Component {
    static Component = null;
    state = { Component: AsyncComponent.Component };

    componentWillMount() {
      if (!this.state.Component) {
        getComponent().then(Component => {
          AsyncComponent.Component = Component;
          this.setState({ Component });
        });
      }
    }
    render() {
      const { Component } = this.state;
      if (Component) {
        return <Component {...this.props} />;
      }
      return null;
    }
  };
}

const Registration = asyncComponent(() =>
  import("./components/registration/registration-component").then(
    module => module.default
  )
);

const Routes = () => {
  return (
    <Router>
      <div>
        <StyledLink to="/" className="link -primary">
          Home
        </StyledLink>
        <Route path="/" exact component={Welcome} />
        <Route path="/registration" component={Registration} />
      </div>
    </Router>
  );
};

export default Routes;

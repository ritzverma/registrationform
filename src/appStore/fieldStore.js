const fieldValues = {
  name: {
    label: "Name",
    validationType: "validateString",
    placeholderText: "Enter your Name",
    validationText: "Enter any character to enable the button",
    dataType: "name",
    type: "text",
    data: ""
  },
  email: {
    label: "Email",
    validationType: "validateEmail",
    placeholderText: "Enter valid Email",
    validationText: "Enter valid email id to enable the button",
    dataType: "email",
    type: "text",
    data: ""
  },
  username: {
    label: "Username",
    validationType: "validateString",
    placeholderText: "Enter your Username",
    validationText: "Enter any character to enable the button",
    dataType: "username",
    type: "text",
    data: ""
  },
  password: {
    label: "Password",
    validationType: "validateString",
    placeholderText: "Enter your Password",
    validationText: "Enter any character to enable the button",
    dataType: "password",
    type: "password",
    data: ""
  },
  phone: {
    label: "Phone",
    validationType: "validateNumeric",
    placeholderText: "Enter valid Phone Number",
    validationText: "Enter only numeric value to enable the button",
    dataType: "phone",
    type: "text",
    data: ""
  }
};

export default fieldValues;

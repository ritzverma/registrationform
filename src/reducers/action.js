import fieldValues from "../appStore/fieldStore";

const reducer = (state = fieldValues, action) => {
  switch (action.type) {
    case "Save_Value":
      state[action.payload.field_type] = {
        ...state[action.payload.field_type],
        data: action.payload.field_data
      };
      break;
  }
  return state;
};

export default reducer;

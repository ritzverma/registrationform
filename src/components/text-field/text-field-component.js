import React from "react";
import Validate from "../../utilities/validator";
import {
  Fieldwrapper,
  Tooltip,
  Formfooter,
  FormInput,
  Button
} from "../custom-components/custom-styled-component";

class TextField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: true
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    const validate = Validate(this.input.value, this.props.data.validationType);
    if (validate && this.input.value != "") {
      this.setState({
        error: false
      });
    } else {
      this.setState({
        error: true
      });
    }
  }

  render() {
    return (
      <div>
        <Fieldwrapper>
          <li>
            <label>{this.props.data.label}</label>
            <Tooltip>
              ?
              <span>{this.props.data.validationText}</span>
            </Tooltip>
            <FormInput
              type={this.props.data.type}
              innerRef={elem => {
                this.input = elem;
              }}
              onChange={this.handleChange}
              placeholder={this.props.data.placeholderText}
            />
          </li>
          {!this.state.error && (
            <Formfooter>
              <Button primary pullRight onClick={this.nextStep.bind(this)}>
                Next
              </Button>
            </Formfooter>
          )}
        </Fieldwrapper>
      </div>
    );
  }

  nextStep(e) {
    e.preventDefault();
    const data = this.input.value;
    this.input.value = "";
    this.setState({
      error: true
    });
    this.props.saveValues(data, this.props.data.dataType);
    this.props.nextStep();
  }
}

export default TextField;

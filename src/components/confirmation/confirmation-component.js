import React from "react";
import {
  Fieldwrapper,
  Tooltip,
  Formfooter,
  Button
} from "../custom-components/custom-styled-component";

class Confirmation extends React.Component {
  render() {
    return (
      <div>
        <h2>Confirm Registration</h2>
        <ul>
          <li>
            <b>Name:</b> {this.props.fieldValues.name.data}
          </li>
          <li>
            <b>Email:</b> {this.props.fieldValues.email.data}
          </li>
          <li>
            <b>Username:</b> {this.props.fieldValues.username.data}
          </li>
          <li>
            <b>Phone:</b> {this.props.fieldValues.phone.data}
          </li>
        </ul>
        <Fieldwrapper>
          <Formfooter>
            <Button primary onClick={this.props.submitRegistration}>
              Submit Registration
            </Button>
          </Formfooter>
        </Fieldwrapper>
      </div>
    );
  }
}

export default Confirmation;

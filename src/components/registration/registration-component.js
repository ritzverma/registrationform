import React from "react";
import { connect } from "react-redux";
import TextField from "../text-field/text-field-component";
import Confirmation from "../confirmation/confirmation-component";
import Success from "../form-success/form-success-component";

class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = { step: 1 };
    this.nextStep = this.nextStep.bind(this);
    this.submitRegistration = this.submitRegistration.bind(this);
  }
  nextStep() {
    this.setState({
      step: this.state.step + 1
    });
  }

  submitRegistration() {
    this.nextStep();
  }

  render() {
    const { step } = this.state;
    const fieldValues = this.props.fieldValues;
    return (
      <main>
        {step < 6 && <h2>Account Details</h2>}
        {step === 1 && (
          <TextField
            data={fieldValues.name}
            fieldValues={fieldValues.name.data}
            nextStep={this.nextStep}
            saveValues={this.props.saveValues}
          />
        )}
        {step === 2 && (
          <TextField
            data={fieldValues.email}
            fieldValues={fieldValues.email.data}
            nextStep={this.nextStep}
            saveValues={this.props.saveValues}
          />
        )}
        {step === 3 && (
          <TextField
            data={fieldValues.username}
            fieldValues={fieldValues.username.data}
            nextStep={this.nextStep}
            saveValues={this.props.saveValues}
          />
        )}
        {step === 4 && (
          <TextField
            data={fieldValues.password}
            fieldValues={fieldValues.password.data}
            nextStep={this.nextStep}
            saveValues={this.props.saveValues}
          />
        )}
        {step === 5 && (
          <TextField
            data={fieldValues.phone}
            fieldValues={fieldValues.phone.data}
            nextStep={this.nextStep}
            saveValues={this.props.saveValues}
          />
        )}
        {step === 6 && (
          <Confirmation
            fieldValues={fieldValues}
            submitRegistration={this.submitRegistration}
          />
        )}
        {step === 7 && <Success fieldValues={fieldValues} />}
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    fieldValues: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveValues: (field_data, field_type) => {
      dispatch({
        type: "Save_Value",
        payload: { field_data, field_type }
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);

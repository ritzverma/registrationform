import styled, { css } from "styled-components";
import { Link } from "react-router-dom";

export const Fieldwrapper = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;

  > li {
    margin-bottom: 20px;
  }
`;
export const Tooltip = styled.label`
  position: relative;
  display: inline-block;
  top: -5px;
  font-size: 12px;
  color: ${props => props.theme.grey};
  left: 5px;

  > span {
    visibility: hidden;
    min-width: 120px;
    background-color: ${props => props.theme.green};
    color: ${props => props.theme.white};
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 0.3s;
    font-size: 10px;
  }
  &:hover > span {
    visibility: visible;
    opacity: 1;
  }
  > span::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: ${props => props.theme.green} transparent transparent
      transparent;
  }
`;
export const Formfooter = styled.li`
  border-top: 1px solid #ddd;
  margin-top: 10px;
  padding-top: 20px;
  text-align: center;
`;
export const FormInput = styled.input`
  font-size: 16px;
  padding: 8px;
  width: 100%;
`;
export const Button = styled.button`
  border-radius: 3px;
  border: none;
  color: #fff;
  cursor: pointer;
  font-size: 16px;
  padding: 10px 12px;
  text-align: center;
  ${props =>
    props.primary &&
    css`
      background-color: ${props => props.theme.blue};
      border: 1px solid ${props => props.theme.blue};
    `};
  ${props =>
    props.default &&
    css`
      background-color: ${props => props.theme.white};
      border: 1px solid ${props => props.theme.grey};
      color: ${props => props.theme.grey};
    `};
  ${props =>
    props.pullRight &&
    css`
      float: right;
    `};
`;

export const Wrapper = styled.div`
  text-align: center;
`;

export const StyledButtonLink = styled(Link)`
  text-decoration: none;
  background-color: ${props => props.theme.blue};
  border: 1px solid ${props => props.theme.blue};
  color: ${props => props.theme.white};
  border-radius: 3px;
  padding: 10px 12px;
`;

export const StyledLink = styled(Link)`
  text-decoration: none;
  color: ${props => props.theme.blue};
`;

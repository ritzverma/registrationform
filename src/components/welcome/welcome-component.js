import React from "react";
import { Link } from "react-router-dom";
import {
  Wrapper,
  StyledButtonLink
} from "../custom-components/custom-styled-component";

class Welcome extends React.Component {
  render() {
    return (
      <div>
        <Wrapper>
          <h1>Welcome to our App!!</h1>
          <h3>You're Just a step away to experience Awesomeness.</h3>
          <br />
          <StyledButtonLink to="/registration">
            Click Here to Register
          </StyledButtonLink>
        </Wrapper>
      </div>
    );
  }
}

export default Welcome;

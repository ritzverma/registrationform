import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import styled, { ThemeProvider } from "styled-components";
import theme from "./themes/appTheme";
import store from "./appStore/DBstore";
import Routes from "./router";

//SCSS files have been replaced by Styled components
// import "./sass/app.scss";

class Home extends React.Component {
  render() {
    return (
      <div>
        <Routes />
      </div>
    );
  }
}

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Home />
    </ThemeProvider>
  </Provider>,
  document.getElementById("app")
);

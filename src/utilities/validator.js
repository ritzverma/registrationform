const Validate = (fieldCheck, validationType) => {
  switch (validationType) {
    case "validateString": {
      if (typeof fieldCheck === "string") {
        return true;
      }
      break;
    }
    case "validateEmail": {
      if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(fieldCheck)) {
        return true;
      }
      break;
    }
    case "validateNumeric": {
      if (!isNaN(parseFloat(fieldCheck))) {
        return true;
      }
      break;
    }
  }
};
export default Validate;

const theme = {
  grey: "#4c5e65",
  green: "#3aad91",
  blue: "#2989c6",
  orange: "#fb6733",
  red: "#d33847",
  white: "#fff"
};
export default theme;

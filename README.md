This application demonstrates Registration form.



Please find below as steps to run and configure application:

1. Please take latest pull and run yarn install on terminal.
2. Once yarn executes successfully, run 'yarn start'.
3. Open URL 'localhost:8080' in browser.
4. This will show up Welcome Page.
5. All validations are applied on button visibility. Button will be visible only if validations are passed.


###To make Production Folder run yarn prodbuild. This will make a dist folder with respective files and run web devpack server from the dist folder itself.

Technology stack used:

1. React

2. Redux

3. Webpack

4. Babel

5. SASS

6. React Router

7. Yarn (Package Manager)